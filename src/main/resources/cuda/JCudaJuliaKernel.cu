extern "C"

__global__ void julia(int n, double *input, double* c, double r, int depth, int* result) {
    //Check conditions
    int ind = blockIdx.x * blockDim.x + threadIdx.x;
    if (ind >= n) {
        return;
    }
    //
    int real_pos = ind * 2;
    int img_pos = real_pos + 1;
    //
    double real_value = input[real_pos];
    double img_value = input[img_pos];
    double r_sqr = r * r;
    int i = 0;
    while ((real_value * real_value + img_value * img_value < r_sqr) && (i < depth)) {
        double new_real_value = real_value * real_value - img_value * img_value + c[0];
        double new_img_value = 2 * real_value * img_value + c[1];
        real_value = new_real_value;
        img_value = new_img_value;
        i++;
    }
    result[ind] = i;
}