package ru.vsu.cs.gorovojab.julia.services;

import org.apache.commons.math3.complex.Complex;

public class OpenMPJuliaService extends OneThreadJuliaService {

    @Override
    protected int[][] generateJulia(Complex c, double r, int depth,
                                    Complex startPoint, Complex xVector, Complex yVector,
                                    int width, int height) {
        int[][] julia = new int[width][height];
        // omp parallel for public(julia)
        for (int i = 0; i < width; i++) {
            Complex localStart = startPoint.add(xVector.multiply(i));
            int[][] localJulia = this.generatePart(c, r, depth, localStart, xVector, yVector, 1, height);
            System.arraycopy(localJulia[0], 0, julia[i], 0, height);
        }
        return julia;
    }

    private int[][] generatePart(Complex c, double r, int depth,
                                 Complex startPoint, Complex xVector, Complex yVector,
                                 int width, int height) {
        return super.generateJulia(c, r, depth, startPoint, xVector, yVector, width, height);
    }
}
