package ru.vsu.cs.gorovojab.julia.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.math3.complex.Complex;

import java.awt.image.BufferedImage;

@Data
@AllArgsConstructor
public class JuliaModel {
    private final BufferedImage image;
    private final Complex leftTopCorner;
    private final Complex rightBottomCorner;
}
