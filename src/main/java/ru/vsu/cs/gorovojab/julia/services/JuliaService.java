package ru.vsu.cs.gorovojab.julia.services;

import org.apache.commons.math3.complex.Complex;
import ru.vsu.cs.gorovojab.julia.model.JuliaCoefficientsModel;
import ru.vsu.cs.gorovojab.julia.model.JuliaModel;
import ru.vsu.cs.gorovojab.julia.util.GeometryUtil;

import java.awt.image.BufferedImage;

public abstract class JuliaService {

    public JuliaModel draw(JuliaCoefficientsModel model, int depth,
                           double rotationAngle,
                           Complex center, Integer k,
                           int width, int height) {
        if (width == 0 || height == 0) {
            return null;
        }
        if (center == null) {
            center = Complex.ZERO;
        }
        //Calculate common values
        Complex c = this.calculateC(model);
        double r = this.calculateR(c);
        //Calculate start point
        double realWidth = r * width / Math.min(width, height) / k;
        double imgWidth = r * height / Math.min(width, height) / k;
        Complex startPoint = center.subtract(new Complex(realWidth, imgWidth));
        //Rotate start point around center
        startPoint = GeometryUtil.rotatePoint(startPoint, rotationAngle, center);
        //
        Complex xVector = new Complex(realWidth * 2 / width, 0);
        xVector = GeometryUtil.rotatePoint(xVector, rotationAngle, Complex.ZERO);
        Complex yVector = new Complex(0, imgWidth * 2 / height);
        yVector = GeometryUtil.rotatePoint(yVector, rotationAngle, Complex.ZERO);
        //Perform julia calculation
        int[][] intensity = this.generateJulia(
                c, r, depth,
                startPoint, xVector, yVector,
                width, height
        );
        return new JuliaModel(
                createImage(intensity, depth),
                new Complex(center.getReal() - realWidth, center.getImaginary() - imgWidth),
                new Complex(center.getReal() + realWidth, center.getImaginary() + imgWidth)
        );
    }

    protected abstract int[][] generateJulia(Complex c, double r, int depth,
                                             Complex startPoint, Complex xVector, Complex yVector,
                                             int width, int height);

    protected int getBreakingIteration(Complex value, Complex c, double r, int depth) {
        for (int i = 0; i < depth; i++) {
            if (value.abs() > r) {
                return i;
            }
            value = value.multiply(value).add(c);
        }
        return depth;
    }

    private double calculateR(Complex c) {
        return (1 + Math.sqrt(1 + 4 * c.abs())) / 2;
    }

    /**
     * Counts coefficient for conversion from <code>f(z)=z<sub>2</sub>+c</code> to <code>f(z)=A*z^2+B*z+D</code>
     * @param model
     * @return
     */
    private Complex calculateC(JuliaCoefficientsModel model) {
        return model.getA().multiply(model.getD())                  // A*D
                .add(model.getB().divide(2))                        // + B/2
                .add(model.getB().multiply(model.getB()).divide(4));// + (B/2)^2
    }

    private BufferedImage createImage(int[][] intensity, int depth) {
        int width = intensity.length;
        int height = intensity[0].length;
        //Looking for max iteration
        int max = 0;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (intensity[i][j] > max) {
                    max = intensity[i][j];
                    if (max == depth) break;
                }
            }
            if (max == depth) break;
        }
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                image.setRGB(i, j, getColor(intensity[i][j], max));
            }
        }
        return image;
    }

    private int getColor(int value, int max) {
        double k = (value * 1.0 / max);
        if (k > 1 || k < 0) {
            throw new RuntimeException("Incorrect max! value / max not in borders: [0; 1]");
        }
        int r = (int) (255 * (k < 0.5 ? 0 : (k - 0.5) * 2));
        int g = (int) (255 * (k > 0.5 ? 1 - k : k) * 2);
        int b = (int) (255 * (k > 0.5 ? 0 : (0.5 - k) * 2));
        return (r << 16 & 0xFF0000) | (g << 8 & 0x00FF00) | (b & 0x0000FF);
    }
}
