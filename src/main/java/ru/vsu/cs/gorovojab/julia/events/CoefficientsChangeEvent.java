package ru.vsu.cs.gorovojab.julia.events;

import javafx.event.Event;
import javafx.event.EventType;
import lombok.Getter;
import ru.vsu.cs.gorovojab.julia.model.JuliaCoefficientsModel;

public class CoefficientsChangeEvent extends Event {

    public final static EventType<CoefficientsChangeEvent> TYPE = new EventType<>(CoefficientsChangeEvent.class.getName());

    @Getter
    private final JuliaCoefficientsModel juliaCoefficients;

    public CoefficientsChangeEvent(JuliaCoefficientsModel juliaCoefficients) {
        super(TYPE);
        this.juliaCoefficients = juliaCoefficients;
    }
}
