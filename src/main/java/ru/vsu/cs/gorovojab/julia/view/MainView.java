package ru.vsu.cs.gorovojab.julia.view;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.Alert;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import lombok.Getter;
import lombok.extern.java.Log;
import org.apache.commons.math3.complex.Complex;
import ru.vsu.cs.gorovojab.julia.events.ControlsChangedEvent;
import ru.vsu.cs.gorovojab.julia.events.SaveEvent;
import ru.vsu.cs.gorovojab.julia.model.JuliaCoefficientsModel;
import ru.vsu.cs.gorovojab.julia.model.JuliaModel;
import ru.vsu.cs.gorovojab.julia.util.GeometryUtil;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

@Log
public class MainView extends GridPane {

    @Getter
    private final ImageView imageView;
    @Getter
    private final ControlPanel controlPanel;

    private JuliaCoefficientsModel currentCoefficients = null;
    private double currentRotationAngle = 0d;
    //
    private Complex center = Complex.ZERO;
    private Complex leftTop = null;
    private Complex rightBottom = null;
    private int scale = 1;

    private void onControlChange(ControlsChangedEvent event) {
        this.redraw(event.getJuliaCoefficientsModel(), event.getRotationAngle());
    }

    public void refresh() {
        this.redraw(this.currentCoefficients, this.currentRotationAngle);
    }

    private void redraw(JuliaCoefficientsModel coefficients, double angle) {
        log.fine("Redraw fractal...");
        //Save last parameters
        this.currentCoefficients = coefficients;
        //Draw
        long start = System.currentTimeMillis();

        JuliaModel julia = this.controlPanel.getService().draw(
                coefficients, 150, angle,
                center, scale,
                (int) imageView.getFitWidth(),
                (int) imageView.getFitHeight()
        );
        if (julia != null) {
            this.imageView.setImage(SwingFXUtils.toFXImage(julia.getImage(), null));
            this.controlPanel.setInfo(
                    julia.getLeftTopCorner(), julia.getRightBottomCorner(), scale,
                    System.currentTimeMillis() - start
            );
            this.leftTop = julia.getLeftTopCorner();
            this.rightBottom = julia.getRightBottomCorner();
            this.currentRotationAngle = angle;
        }
    }

    public void onImageClicked(MouseEvent event) {
        if (event.getButton().equals(MouseButton.PRIMARY)) {
            //Zoom in
            double clickX = event.getX() / (((ImageView)event.getSource()).getFitWidth()) - 0.5;
            double clickY = event.getY() / (((ImageView)event.getSource()).getFitHeight()) - 0.5;
            Complex viewBorders = rightBottom.subtract(leftTop);
            Complex newCenter = new Complex(
                    center.getReal() + viewBorders.getReal() * clickX,
                    center.getImaginary() + viewBorders.getImaginary() * clickY
            );
            //Rotate corresponding to current rotation
            center = GeometryUtil.rotatePoint(newCenter, this.currentRotationAngle, center);
            //Increase scale
            scale *= 2;
        } else {
            //Zoom out
            if (scale == 1) return;
            scale /= 2;
        }
        this.refresh();
    }

    public void onImageSave(SaveEvent event) {
        String fileName = event.getFileName();
        String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
        try {
            BufferedImage src = SwingFXUtils.fromFXImage(this.imageView.getImage(), null);
            BufferedImage convertedImg = new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_INT_RGB);
            convertedImg.getGraphics().drawImage(src, 0, 0, null);
            ImageIO.write(convertedImg, extension, new File(fileName));
        } catch (IOException e) {
            log.log(Level.SEVERE, "Image was not saved", e);
        }
    }

    public MainView() {
        super();
        //
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setHgrow(Priority.ALWAYS);
        this.getColumnConstraints().add(column1);
        //
        this.imageView = new ImageView();
        this.imageView.setPreserveRatio(true);
        this.imageView.setOnMouseClicked(this::onImageClicked);
        BorderPane imagePane = new BorderPane();
        imagePane.setCenter(imageView);
        this.add(imagePane, 0, 0);
        //
        this.controlPanel = new ControlPanel();
        this.controlPanel.addEventHandler(ControlsChangedEvent.TYPE, this::onControlChange);
        this.controlPanel.addEventHandler(SaveEvent.TYPE, this::onImageSave);
        this.add(controlPanel, 1, 0);
        //
        this.imageView.fitHeightProperty().bind(this.heightProperty());
        this.imageView.fitWidthProperty().bind(this.widthProperty().subtract(this.controlPanel.minWidthProperty()));
        this.controlPanel.prefHeightProperty().bind(this.heightProperty());
        //Set default values
        this.controlPanel.setValues(new JuliaCoefficientsModel(Complex.ONE, Complex.ZERO, new Complex(-0.74543, 0.11301)));
    }
}
