package ru.vsu.cs.gorovojab.julia.util;

import org.apache.commons.math3.complex.Complex;

public class GeometryUtil {

    public static Complex rotatePoint(Complex point, double angle, Complex center) {
        return new Complex(
                center.getReal() + (point.getReal() - center.getReal())*Math.cos(angle) - (point.getImaginary() - center.getImaginary())*Math.sin(angle),
                center.getImaginary() + (point.getReal() - center.getReal())*Math.sin(angle) + (point.getImaginary() - center.getImaginary())*Math.cos(angle)
        );
    }
}
