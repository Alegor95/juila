package ru.vsu.cs.gorovojab.julia.events;

import javafx.event.Event;
import javafx.event.EventType;
import lombok.Getter;
import ru.vsu.cs.gorovojab.julia.model.JuliaCoefficientsModel;

public class ControlsChangedEvent extends Event {

    public static final EventType<ControlsChangedEvent> TYPE = new EventType<>(ControlsChangedEvent.class.getName());

    @Getter
    private final JuliaCoefficientsModel juliaCoefficientsModel;
    @Getter
    private final double rotationAngle;

    public ControlsChangedEvent(JuliaCoefficientsModel juliaCoefficients, double rotationAngle) {
        super(TYPE);
        this.juliaCoefficientsModel = juliaCoefficients;
        this.rotationAngle = rotationAngle;
    }
}
