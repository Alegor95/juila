package ru.vsu.cs.gorovojab.julia.services;

import org.apache.commons.math3.complex.Complex;

public class OneThreadJuliaService extends JuliaService {

    protected int[][] generateJulia(Complex c, double r, int depth,
                                  Complex startPoint, Complex xVector, Complex yVector,
                                  int width, int height) {
        //Fill step maps
        int[][] result = new int[width][height];
        Complex rowStart = startPoint;
        Complex value = rowStart;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                result[i][j] = getBreakingIteration(value, c, r, depth);
                value = value.add(yVector);
            }
            rowStart = rowStart.add(xVector);
            value = rowStart;
        }
        return result;
    }

}
