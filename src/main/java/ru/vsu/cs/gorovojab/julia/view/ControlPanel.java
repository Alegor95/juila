package ru.vsu.cs.gorovojab.julia.view;

import javafx.event.EventHandler;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import org.apache.commons.math3.complex.Complex;
import ru.vsu.cs.gorovojab.julia.events.CoefficientsChangeEvent;
import ru.vsu.cs.gorovojab.julia.events.ControlsChangedEvent;
import ru.vsu.cs.gorovojab.julia.events.SaveEvent;
import ru.vsu.cs.gorovojab.julia.model.JuliaCoefficientsModel;
import ru.vsu.cs.gorovojab.julia.services.JuliaService;

public class ControlPanel extends GridPane {

    private final CoefficientsPanel coefficientsPanel;
    private final CurrentBordersPanel currentBordersPanel;
    private final RotationPanel rotationPanel;
    private final ServicePanel servicePanel;
    private final SavePanel savePanel;

    private void onCoefficientsChanged(CoefficientsChangeEvent event) {
        this.fireEvent(new ControlsChangedEvent(
                event.getJuliaCoefficients(),
                this.rotationPanel.getAngle()
        ));
    }

    public JuliaService getService() {
        return this.servicePanel.getCurrentService();
    }

    public void setInfo(Complex leftTop, Complex rightBottom, int scale, long time) {
        this.currentBordersPanel.setLeftTopCorner(leftTop);
        this.currentBordersPanel.setRightBottomCorner(rightBottom);
        this.currentBordersPanel.setScale(scale);
        this.currentBordersPanel.setLastTime(time);
    }

    public void setValues(JuliaCoefficientsModel model) {
        this.coefficientsPanel.setModel(model);
    }

    public ControlPanel() {
        super();
        this.setBackground(new Background(
                new BackgroundFill(Color.LIGHTGOLDENRODYELLOW, null, null)
        ));
        int currentRow = 0;
        //
        this.currentBordersPanel = new CurrentBordersPanel();
        this.add(this.currentBordersPanel, 0, currentRow++);
        //
        this.rotationPanel = new RotationPanel();
        this.add(this.rotationPanel, 0, currentRow++);
        //
        this.servicePanel = new ServicePanel();
        this.add(this.servicePanel, 0, currentRow++);
        //
        this.coefficientsPanel = new CoefficientsPanel();
        this.addEventFilter(CoefficientsChangeEvent.TYPE, this::onCoefficientsChanged);
        this.add(this.coefficientsPanel, 0, currentRow++);
        //
        this.savePanel = new SavePanel();
        savePanel.addEventHandler(SaveEvent.TYPE, this::fireEvent);
        this.add(this.savePanel, 0, currentRow++);
        //
        this.setMinWidth(200);
        this.setMaxWidth(200);
    }
}
