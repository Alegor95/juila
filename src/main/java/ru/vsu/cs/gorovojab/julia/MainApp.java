package ru.vsu.cs.gorovojab.julia;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import jcuda.driver.JCudaDriver;
import ru.vsu.cs.gorovojab.julia.view.MainView;

public class MainApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) {
        JCudaDriver.setExceptionsEnabled(true);
        MainView view = new MainView();
        primaryStage.setScene(new Scene(view, 700, 500));
        primaryStage.setTitle("Julia");
        primaryStage.setOnShowing(event -> view.refresh());
        primaryStage.show();
    }
}
