package ru.vsu.cs.gorovojab.julia.services;

import jcuda.Pointer;
import jcuda.Sizeof;
import jcuda.driver.*;
import org.apache.commons.math3.complex.Complex;
import ru.vsu.cs.gorovojab.julia.util.CudaUtils;

import java.io.IOException;

public class CudaJuliaService extends JuliaService {

    @Override
    protected int[][] generateJulia(Complex c, double r, int depth,
                                    Complex startPoint, Complex xVector, Complex yVector,
                                    int width, int height) {

        // Initialize the driver and create a context for the first device.
        JCudaDriver.cuInit(0);
        CUdevice device = new CUdevice();
        JCudaDriver.cuDeviceGet(device, 0);
        CUcontext context = new CUcontext();
        JCudaDriver.cuCtxCreate(context, 0, device);
        // Load the ptx file.
        // Read the PTX data into a zero-terminated string byte array
        byte ptxData[] = new byte[0];
        try {
            ptxData = CudaUtils.toZeroTerminatedStringByteArray(
                    CudaJuliaService.class.getResourceAsStream("/cuda/JCudaJuliaKernel.ptx")
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        CUmodule module = new CUmodule();
        JCudaDriver.cuModuleLoadData(module, ptxData);
        // Obtain a function pointer to the "julia" function.
        CUfunction function = new CUfunction();
        JCudaDriver.cuModuleGetFunction(function, module, "julia");
        int numElements = width * height;
        //Prepare data
        double[] elements = new double[numElements * 2];
        Complex rowStart = startPoint;
        Complex value = rowStart;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int ind = i * height + j;
                //
                elements[ind * 2 + 0] = value.getReal();
                elements[ind * 2 + 1] = value.getImaginary();
                //
                value = value.add(yVector);
            }
            rowStart = rowStart.add(xVector);
            value = rowStart;
        }
        // Allocate the device input data, and copy the
        // host input data to the device
        CUdeviceptr deviceInputA = new CUdeviceptr();
        JCudaDriver.cuMemAlloc(deviceInputA, numElements * 2 * Sizeof.DOUBLE);
        JCudaDriver.cuMemcpyHtoD(deviceInputA, Pointer.to(elements), numElements * 2 * Sizeof.DOUBLE);
        // C input
        CUdeviceptr deviceInputC = new CUdeviceptr();
        JCudaDriver.cuMemAlloc(deviceInputC, 2 * Sizeof.DOUBLE);
        JCudaDriver.cuMemcpyHtoD(deviceInputC, Pointer.to(new double[] { c.getReal(), c.getImaginary() }), 2 * Sizeof.DOUBLE);
        // Output data
        CUdeviceptr deviceOutput = new CUdeviceptr();
        JCudaDriver.cuMemAlloc(deviceOutput, numElements * Sizeof.INT);
        // Set up the kernel parameters: A pointer to an array
        // of pointers which point to the actual values.
        Pointer kernelParameters = Pointer.to(
                Pointer.to(new int[] { numElements }),
                Pointer.to(deviceInputA),
                Pointer.to(deviceInputC),
                Pointer.to(new double[] { r }),
                Pointer.to(new int[] { depth }),
                Pointer.to(deviceOutput)
        );

        // Call the kernel function.
        int blockSizeX = 256;
        int gridSizeX = (int)Math.ceil(numElements * 2. / blockSizeX);
        JCudaDriver.cuLaunchKernel(function,
                gridSizeX,  1, 1,      // Grid dimension
                blockSizeX, 1, 1,      // Block dimension
                0, null,               // Shared memory size and stream
                kernelParameters, null // Kernel- and extra parameters
        );
        JCudaDriver.cuCtxSynchronize();

        // Allocate host output memory and copy the device output
        // to the host.
        int hostOutput[] = new int[numElements];
        JCudaDriver.cuMemcpyDtoH(Pointer.to(hostOutput), deviceOutput, numElements * Sizeof.INT);

        // Clean up.
        JCudaDriver.cuMemFree(deviceInputA);
        JCudaDriver.cuMemFree(deviceInputC);
        JCudaDriver.cuMemFree(deviceOutput);
        //Convert to rectangle array
        int[][] output = new int[width][height];
        for (int i = 0; i < width; i++) {
            System.arraycopy(hostOutput, i * height, output[i], 0, height);
        }
        return output;
    }

}
