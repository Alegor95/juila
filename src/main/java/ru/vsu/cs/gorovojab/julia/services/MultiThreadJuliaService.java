package ru.vsu.cs.gorovojab.julia.services;

import org.apache.commons.math3.complex.Complex;

import java.util.concurrent.CompletableFuture;

public class MultiThreadJuliaService extends OneThreadJuliaService {

    private final int threadCount;

    @Override
    protected int[][] generateJulia(Complex c, double r, int depth,
                                    Complex startPoint, Complex xVector, Complex yVector,
                                    int width, int height) {
        //Count rows for each thread
        int[] columnCount = this.countDistribution(this.threadCount, width);
        int[] columnShift = this.countShifts(columnCount);
        Complex[] startPoints = this.generateStartPoints(columnShift, startPoint, xVector);
        //Result
        int[][] julia = new int[width][height];
        //Fill tasks
        CompletableFuture[] tasks = new CompletableFuture[columnCount.length];
        for (int i = 0; i < columnCount.length; i++) {
            int partWidth = columnCount[i];
            int partShift = columnShift[i];
            Complex partStart = startPoints[i];
            tasks[i] = CompletableFuture.runAsync(() -> {
                int[][] partJulia = super.generateJulia(
                        c, r, depth,
                        partStart, xVector, yVector,
                        partWidth, height
                );
                for (int column = partShift; column < partShift + partWidth; column++) {
                    System.arraycopy(
                            partJulia[column - partShift], 0,
                            julia[column], 0, height
                    );
                }
            });
        }
        CompletableFuture.allOf(tasks).join();
        //
        return julia;
    }

    private Complex[] generateStartPoints(int[] shifts, Complex startPoint, Complex shiftVector) {
        Complex[] startPoints = new Complex[shifts.length];
        for (int i = 0; i < shifts.length; i++) {
            startPoints[i] = startPoint.add(shiftVector.multiply(shifts[i]));
        }
        return startPoints;
    }

    private int[] countShifts(int[] rowCount) {
        int[] rowShift = new int[rowCount.length];
        int currentRow = 0;
        for (int i = 0; i < rowCount.length; i++) {
            rowShift[i] = currentRow;
            currentRow += rowCount[i];
        }
        return rowShift;
    }

    private int[] countDistribution(int threadCount, int totalRows) {
        int[] rowCount = new int[threadCount];
        int reminder = totalRows % threadCount;
        int base = totalRows / threadCount;
        for (int i = 0; i < threadCount; i++) {
            rowCount[i] = base;
            if (reminder > 0) {
                rowCount[i]++;
                reminder--;
            }
        }
        return rowCount;
    }

    public MultiThreadJuliaService(int threadCount) {
        this.threadCount = threadCount;
    }
}
