package ru.vsu.cs.gorovojab.julia.view;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import lombok.Getter;
import org.apache.commons.math3.complex.Complex;

public class CurrentBordersPanel extends GridPane {

    private final static String X_BORDERS_TEXT = "X: [%e, %e]";
    private final static String Y_BORDERS_TEXT = "Y: [%e, %e]";
    private final static String SCALE_TEXT = "Scale: x%d";
    private final static String TIME_TEXT = "Last execution: %d ms";

    @Getter
    private Complex leftTopCorner;
    @Getter
    private Complex rightBottomCorner;
    @Getter
    private int scale = 1;
    @Getter
    private long lastTime = 0;
    //Layout
    private Text xBorders;
    private Text yBorders;
    private Text scaleText;
    private Text timeText;

    public void setLeftTopCorner(Complex leftTopCorner) {
        this.leftTopCorner = leftTopCorner;
        this.updateText();
    }

    public void setRightBottomCorner(Complex rightBottomCorner) {
        this.rightBottomCorner = rightBottomCorner;
        this.updateText();
    }

    public void setScale(int scale) {
        this.scale = scale;
        this.updateText();
    }

    public void setLastTime(long lastTime) {
        this.lastTime = lastTime;
        this.updateText();
    }

    private void updateText() {
        if (leftTopCorner == null || rightBottomCorner == null) {
            this.xBorders.setText(String.format(X_BORDERS_TEXT, Double.NaN, Double.NaN));
            this.yBorders.setText(String.format(Y_BORDERS_TEXT, Double.NaN, Double.NaN));
            this.scaleText.setText(String.format(SCALE_TEXT, scale));
            this.timeText.setText(String.format(TIME_TEXT, lastTime));
        } else {
            this.xBorders.setText(String.format(X_BORDERS_TEXT, leftTopCorner.getReal(), rightBottomCorner.getReal()));
            this.yBorders.setText(String.format(Y_BORDERS_TEXT, leftTopCorner.getImaginary(), rightBottomCorner.getImaginary()));
            this.scaleText.setText(String.format(SCALE_TEXT, scale));
            this.timeText.setText(String.format(TIME_TEXT, lastTime));
        }
    }

    public CurrentBordersPanel() {
        super();
        //Header
        Label header = new Label("Current borders:");
        header.setFont(Font.font(null, FontWeight.BOLD, 12));
        header.setAlignment(Pos.CENTER);
        header.prefWidthProperty().bind(this.widthProperty());
        this.add(header, 0, 0);
        //
        this.xBorders = new Text();
        this.add(this.xBorders, 0, 1);
        //
        this.yBorders = new Text();
        this.add(this.yBorders, 0, 2);
        //
        this.scaleText = new Text();
        this.add(this.scaleText, 0, 3);
        //
        this.timeText = new Text();
        this.add(this.timeText, 0, 4);
        //
        this.updateText();
    }
}