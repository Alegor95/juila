package ru.vsu.cs.gorovojab.julia.view;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import lombok.Getter;
import org.apache.commons.math3.complex.Complex;
import ru.vsu.cs.gorovojab.julia.events.CoefficientsChangeEvent;
import ru.vsu.cs.gorovojab.julia.model.JuliaCoefficientsModel;

import javax.annotation.PostConstruct;

public class CoefficientsPanel extends GridPane {

    //A Coefficient
    private TextField aRealText;
    private TextField aImgText;

    //B Coefficient
    private TextField bRealText;
    private TextField bImgText;

    //D Coefficient
    private TextField dRealText;
    private TextField dImgText;

    //Apply button
    private Button applyButton;

    @Getter
    private JuliaCoefficientsModel model;

    private Double extractNumber(TextField field) {
        String stringValue = field.getText();
        Double value = null;
        try {
            value = Double.valueOf(stringValue);
            field.setStyle("");
        } catch (NumberFormatException e) {
            field.setStyle("-fx-control-inner-background: #ffbbbb");
        }
        return value;
    }

    private void onApply(ActionEvent e) {
        //Validate input and pass it to subscribers
        Double aReal = this.extractNumber(this.aRealText);
        Double aImg = this.extractNumber(this.aImgText);
        Double bReal = this.extractNumber(this.bRealText);
        Double bImg = this.extractNumber(this.bImgText);
        Double dReal = this.extractNumber(this.dRealText);
        Double dImg = this.extractNumber(this.dImgText);
        if (aReal == null || aImg == null
                || bReal == null || bImg == null
                || dReal == null || dImg == null) {
            new Alert(
                    Alert.AlertType.WARNING,
                    "One or more parameter was incorrect! Enter correct numbers"
            ).show();
            return;
        }
        JuliaCoefficientsModel newModel = new JuliaCoefficientsModel(
                new Complex(aReal, aImg), new Complex(bReal, bImg), new Complex(dReal, dImg)
        );
        this.setModel(newModel);
    }

    public void setModel(JuliaCoefficientsModel model) {
        this.model = model;
        this.aRealText.setText(Double.toString(model.getA().getReal()));
        this.aImgText.setText(Double.toString(model.getA().getImaginary()));
        this.bRealText.setText(Double.toString(model.getB().getReal()));
        this.bImgText.setText(Double.toString(model.getB().getImaginary()));
        this.dRealText.setText(Double.toString(model.getD().getReal()));
        this.dImgText.setText(Double.toString(model.getD().getImaginary()));
        this.fireEvent(new CoefficientsChangeEvent(model));
    }

    private void addCoefficient(int baseRow, String coefficientName,
                           TextField realText, TextField imgText) {
        //Set real properties
        realText.setText("0");
        //Set img properties
        imgText.setText("0");
        //Generate layout
        this.add(new Text(coefficientName + ":"), 0, baseRow);
        //
        this.add(realText, 1, baseRow);
        this.add(new Text("+ I*"), 2, baseRow);
        //
        this.add(imgText, 3, baseRow);
    }

    public CoefficientsPanel() {
        super();
        this.setHgap(5);
        this.setVgap(5);
        this.setPadding(new Insets(0, 5, 0, 5));
        //Add children
        int currentRow = 0;
        //Header
        Label header = new Label("f(z) = A*z^2 + B*z + D");
        header.setFont(Font.font(null, FontWeight.BOLD, 12));
        header.setAlignment(Pos.CENTER);
        header.setMaxWidth(Double.MAX_VALUE);
        this.add(header, 0, currentRow++, 4, 1);
        // A
        this.aRealText = new TextField();
        this.aImgText = new TextField();
        this.addCoefficient(currentRow++, "A", this.aRealText, this.aImgText);
        //B
        this.bRealText = new TextField();
        this.bImgText = new TextField();
        this.addCoefficient(currentRow++, "B", this.bRealText, this.bImgText);
        //D
        this.dRealText = new TextField();
        this.dImgText = new TextField();
        this.addCoefficient(currentRow++, "D", this.dRealText, this.dImgText);
        //Apply button
        this.applyButton = new Button("Apply");
        this.applyButton.setOnAction(this::onApply);
        this.applyButton.setMaxWidth(Double.MAX_VALUE);
        this.add(applyButton, 0, currentRow,  4, 1);
    }
}
