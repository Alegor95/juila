package ru.vsu.cs.gorovojab.julia.view;

import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import ru.vsu.cs.gorovojab.julia.services.*;

import java.util.HashMap;
import java.util.Map;

public class ServicePanel extends GridPane {

    private Map<String, JuliaService> serviceMap = new HashMap<>();
    private final ChoiceBox<String> serverSelector;

    public JuliaService getDefaultService() {
        return this.serviceMap.entrySet().iterator().next().getValue();
    }

    public JuliaService getCurrentService() {
        return this.serviceMap.getOrDefault(this.serverSelector.getValue(), getDefaultService());
    }

    private void addService(JuliaService juliaService) {
        serviceMap.put(juliaService.getClass().getSimpleName(), juliaService);
    }

    public ServicePanel() {
        super();
        //Fill map of service
        addService(new OneThreadJuliaService());
        addService(new MultiThreadJuliaService(4));
        addService(new OpenMPJuliaService());
        addService(new CudaJuliaService());
        //Add layout
        int currentRow = 0;
        //Header
        Label header = new Label("Service for computing:");
        header.setFont(Font.font(null, FontWeight.BOLD, 12));
        header.setAlignment(Pos.CENTER);
        header.prefWidthProperty().bind(this.widthProperty());
        this.add(header, 0, currentRow++);
        //Add selector of services
        this.serverSelector = new ChoiceBox<>(FXCollections.observableArrayList(this.serviceMap.keySet()));
        this.serverSelector.getSelectionModel().selectFirst();
        this.serverSelector.prefWidthProperty().bind(this.widthProperty());
        this.add(this.serverSelector, 0, currentRow++);
    }

}
