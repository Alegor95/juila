package ru.vsu.cs.gorovojab.julia.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.math3.complex.Complex;

@Data
@AllArgsConstructor
public class JuliaCoefficientsModel {
    private final Complex a;
    private final Complex b;
    private final Complex d;
}
